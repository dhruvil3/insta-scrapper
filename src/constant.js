export const NUMBER_OF_POST_TO_FETCH = 1;
export const NUMBER_OF_LIKERS_DATA_TO_FETCH = 50;
export const NUMBER_OF_COMMENTERS_DATA_TO_FETCH = 50;

export const WAIT_TIME_FOR_CLICK_EVENT = 750;
export const WAIT_TIME_FOR_SCROLL_EVENT = 750; // Loads elements
export const WAIT_TIME_FOR_MOUSE_HOVER_EVENT = 1000; // Fires query
export const WHILE_LOOP_WAIT_TIME = 100;

export const SELECTORS = {
  headerDiv: '#react-root > section > main > div > header',

  postsDiv: '#react-root > section > main > div > div._2z6nI > article > div:nth-child(1) > div',

  postLikeButton: 'body > div._2dDPU.CkGkG > div.zZYga > div > article > div.eo2As > section.ltpMr.Slqrh > span.fr66n > button',

  postLikes: 'body > div._2dDPU.CkGkG > div.zZYga > div > article > div.eo2As > section.EDfFK.ygqzn > div > div.Nm9Fw > a > span',
  postCloseButton: 'body > div._2dDPU.CkGkG > div.Igw0E.IwRSH.eGOV_._4EzTm.BI4qX.qJPeX.fm1AK.TxciK.yiMZG > button',

  postComments: 'body > div._2dDPU.CkGkG > div.zZYga > div > article > div.eo2As > div.EtaWk > ul',
  commentLoadButton: 'body > div._2dDPU.CkGkG > div.zZYga > div > article > div.eo2As > div.EtaWk > ul > li > div > button',

  likesPage: 'body > div.RnEpo.Yx5HN > div > div > div.Igw0E.IwRSH.eGOV_.vwCYk.i0EQd > div',
  likesPageCloseButton: 'body > div.RnEpo.Yx5HN > div > div > div:nth-child(1) > div > div:nth-child(3) > button',

  mouseOverUserProfile: 'body > div.GdeD6.AzWhO',
  userPostDiv: 'body > div.GdeD6.AzWhO > div > div > div.Igw0E.IwRSH.eGOV_.ybXk5._4EzTm.YlhBV',
  userProfileDiv: 'body > div.GdeD6.AzWhO > div > div > div:nth-child(1)',
};
