chrome.devtools.network.onRequestFinished.addListener(request => {
  request.getContent((body) => {
    if (request.request && request.request.url) {
      if (body) {
        try {
          const responseBody = JSON.parse(body);
          if (responseBody.data) {
            if (responseBody.data.user) {
              if ((responseBody.data.user.edge_owner_to_timeline_media.edges).length > 0) {
                chrome.runtime.sendMessage({
                  message: 'FETCH_LOCATIONS',
                  body: responseBody,
                  url: request.request.url
                });
              }
            }
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
  });
});