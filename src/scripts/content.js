import {
  SELECTORS,
  NUMBER_OF_POST_TO_FETCH,
  NUMBER_OF_LIKERS_DATA_TO_FETCH,
  NUMBER_OF_COMMENTERS_DATA_TO_FETCH,
  WAIT_TIME_FOR_CLICK_EVENT,
  WAIT_TIME_FOR_MOUSE_HOVER_EVENT
} from './../constant';

import {
  selectElement,
  waitForTime,
  mouseEvent,
  scrollAndWaitFotTime,
  clickSelector,
  waitForElementToOpen,
  waitForElementToClose,
  compareFirstChildAndScroll
} from './../helper';

const fetchUserData = async () => {
  const userData = {};
  // Get user's location data from background script
  await new Promise((resolve, reject) => {
    chrome.runtime.sendMessage({ message: 'GET_LOCATIONS' }, (response) => {
      userData.locations = response;
      resolve();
    });
  });
  // Fetch user's profile picture
  const userProfile = selectElement(SELECTORS.userProfileDiv);
  userData.profilePicUrl = userProfile.querySelector('img').src;
  return userData;
}

const hoverAndFetchUserDataIfAccountIsPublic = async (element) => {
  let fetchData = {};

  const mouseOverEvent = mouseEvent('mouseover');
  const mouseOutEvent = mouseEvent('mouseout');

  // Hover mouse pointer over the element
  element.dispatchEvent(mouseOverEvent);
  await waitForElementToOpen(SELECTORS.mouseOverUserProfile);
  await waitForTime(WAIT_TIME_FOR_MOUSE_HOVER_EVENT);
  // Eliminate users with no post or private account
  const accountPublic = selectElement(SELECTORS.userPostDiv);
  if (accountPublic) {
    fetchData = await fetchUserData();
  }
  // Remove mouse pointer over the element
  element.dispatchEvent(mouseOutEvent);
  await waitForElementToClose(SELECTORS.mouseOverUserProfile);
  await waitForTime(WAIT_TIME_FOR_MOUSE_HOVER_EVENT);
  return fetchData;
}

const fetchLikesDataOfPost = async (likeElement) => {
  const likesData = [];
  const uniqueLikes = [];
  while (likesData.length < NUMBER_OF_LIKERS_DATA_TO_FETCH) {

    const likes = likeElement.querySelectorAll('a.FPmhX.notranslate.MBL3Z');
    for (const like of likes) {

      if (!uniqueLikes.includes(like.href)) {
        uniqueLikes.push(like.href);
        const userData = await hoverAndFetchUserDataIfAccountIsPublic(like);
        // If user is public then push data to list
        if (userData.locations && userData.locations.length !== 0) {
          likesData.push({
            profileUrl: like.href,
            profilePicUrl: userData.profilePicUrl,
            locations: userData.locations
          });
        }
        if (likesData.length === NUMBER_OF_LIKERS_DATA_TO_FETCH) break;
      }
    }
    await compareFirstChildAndScroll(likeElement);
  }
  await clickSelector(SELECTORS.likesPageCloseButton);
  return likesData;
}

const fetchCommentDataOfPost = async (commentElement) => {
  const commentsData = [];
  const uniqueComments = [];
  while (commentsData.length < NUMBER_OF_COMMENTERS_DATA_TO_FETCH) {
    // Fetch data from comment section and add all commenters to list
    const comments = commentElement.querySelectorAll('a.sqdOP.yWX7d._8A5w5.ZIAjV');
    for (const comment of comments) {

      if (!uniqueComments.includes(comment.href)) {
        uniqueComments.push(comment.href);
        const userData = await hoverAndFetchUserDataIfAccountIsPublic(comment);
        // If user is public then push data to list
        if (userData.locations && userData.locations.length !== 0) {
          commentsData.push({
            profileUrl: comment.href,
            profilePicUrl: userData.profilePicUrl,
            locations: userData.locations
          });
        }
        if (commentsData.length === NUMBER_OF_COMMENTERS_DATA_TO_FETCH) break;
      }
    }
    await waitForElementToOpen(SELECTORS.commentLoadButton);
    await clickSelector(SELECTORS.commentLoadButton);
  }
  return commentsData;
}

const fetchLikesAndCommentsData = async () => {

  // Get likers list
  await clickSelector(SELECTORS.postLikes);
  const likeElement = selectElement(SELECTORS.likesPage);
  const likesData = await fetchLikesDataOfPost(likeElement);

  // Get commenters list
  await waitForElementToOpen(SELECTORS.postComments);
  const commentElement = selectElement(SELECTORS.postComments);
  const commentsData = await fetchCommentDataOfPost(commentElement);

  return {
    likesData,
    commentsData
  };
}

const fetchSinglePostData = async (post, postHoverDiv) => {
  const element = postHoverDiv.querySelectorAll('li.-V_eO');
  const posturl = post.href;
  const likesCount = element[0].innerText;
  const commentsCount = element[1].innerText;

  await waitForTime(WAIT_TIME_FOR_CLICK_EVENT);
  post.click();
  await waitForTime(WAIT_TIME_FOR_CLICK_EVENT);

  await waitForElementToOpen(SELECTORS.postLikes);
  // Press on like button
  await clickSelector(SELECTORS.postLikeButton);

  const { likesData, commentsData } = await fetchLikesAndCommentsData();

  await waitForElementToOpen(SELECTORS.postCloseButton);
  await clickSelector(SELECTORS.postCloseButton);

  return {
    posturl,
    likesCount,
    commentsCount,
    likesData,
    commentsData
  };
}

const fetchDataFromPostIfNotVideo = async (post) => {
  let fetchedData = {};

  const mouseOverEvent = mouseEvent('mouseover');
  const mouseOutEvent = mouseEvent('mouseout');

  post.dispatchEvent(mouseOverEvent);
  await waitForTime(WAIT_TIME_FOR_MOUSE_HOVER_EVENT);

  const postHoverDiv = post.querySelector('div.qn-0x');
  // If post hover div contains video play icon then post is a video
  const isPostVideo = postHoverDiv.querySelector('span._1P1TY.coreSpritePlayIconSmall');
  if (!isPostVideo) {
    fetchedData = await fetchSinglePostData(post, postHoverDiv);
  }

  post.dispatchEvent(mouseOutEvent);
  await waitForTime(WAIT_TIME_FOR_MOUSE_HOVER_EVENT);

  return fetchedData;
}

const fetchPostsData = async (postDiv) => {
  const uniquePosts = [];
  const postsData = [];
  while (postsData.length < NUMBER_OF_POST_TO_FETCH) {

    // Fetch data from post and add all posts to post list
    const posts = postDiv.querySelectorAll('a');
    let count = 1;
    for (const post of posts) {
      if (!uniquePosts.includes(post.href)) {
        console.log(`Fetching Data For Post ${count}`);

        const fetchedPost = await fetchDataFromPostIfNotVideo(post);
        uniquePosts.push(post.href);
        if (fetchedPost.posturl) postsData.push(fetchedPost);

        console.log(`Data For Post ${count++} Fetched`);
        console.log(fetchedPost);
        if (postsData.length === NUMBER_OF_POST_TO_FETCH) break;
      }
    }
    await scrollAndWaitFotTime(window, 1000, 500);
  }
  return postsData;
}

const fetchProfileInfo = async (headerDiv) => {
  console.log('Fetching Profile Data');

  const profilePicUrl = headerDiv.querySelector('img').src;
  const profileName = headerDiv.querySelector('h2').innerText;
  const profileFollowData = headerDiv.querySelector('ul').innerText;

  const profileData = profileFollowData.split('\n');

  console.log('Profile Data Fetched');

  return {
    profilePicUrl,
    profileName,
    profileData
  };
}

const fetchData = async () => {
  try {
    let profileInfo;
    let posts;

    // Get user's profile data
    await waitForElementToOpen(SELECTORS.headerDiv);
    const headerDiv = selectElement(SELECTORS.headerDiv);
    profileInfo = await fetchProfileInfo(headerDiv);

    // Get user's posts data
    await waitForElementToOpen(SELECTORS.postsDiv);
    const postDiv = selectElement(SELECTORS.postsDiv);
    posts = await fetchPostsData(postDiv);

    return {
      profileInfo,
      posts
    };
  } catch (error) {
    throw error;
  }
}

chrome.runtime.onMessage.addListener((request, sender, response) => {
  if (request.message === 'FETCH_DATA') {
    fetchData()
      .then((result) => {
        console.log(result);
        response('Data Fetched');
      })
      .catch((error) => {
        console.log(error);
        response('Something went wrong');
      });
    return true;
  }
});