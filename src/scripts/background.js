let locations = [];

chrome.runtime.onMessage.addListener((request, sender, response) => {
  // Get xhr data from panel script and fetch locations
  if (request.message === 'FETCH_LOCATIONS') {
    const { body, url } = request;
    console.log(url);
    locations = fetchLocationFromBody(body);
    if (locations.length !== 0) console.log(locations);
    console.log('Location Fetched');
    response('Locations fetched');
  }
  // Send location data to content script
  if (request.message === 'GET_LOCATIONS') {
    console.log('Get Location');
    response(locations);
    locations = [];
  }
});

const fetchLocationFromBody = (body) => {
  const locations = [];
  const posts = body.data.user.edge_owner_to_timeline_media.edges;
  for (const post of posts) {
    if (post.node.location) {
      const location = post.node.location.name;
      locations.push(location);
    }
  }
  return locations;
}