const fetchData = () => {
	chrome.tabs.query({currentWindow: true, active: true}, (tabs) => {
		chrome.tabs.sendMessage(tabs[0].id, { message: 'FETCH_DATA' }, (response) => {
			console.log(response);
		});
	})
}

document.querySelector('#fetch_button').addEventListener('click', fetchData, false);
