import {
  WAIT_TIME_FOR_CLICK_EVENT,
  WAIT_TIME_FOR_SCROLL_EVENT,
  WHILE_LOOP_WAIT_TIME
} from './constant';

export const selectElement = (selector) => document.querySelector(selector);

export const waitForTime = (time) => new Promise(resolve => setTimeout(resolve, time));

export const mouseEvent = (eventName) => new Event(eventName, {
  'view': window,
  'bubbles': true,
  'cancelable': true
});

export const scrollAndWaitFotTime = async (element, scrollHeight, time) => {
  element.scrollBy({
    top: scrollHeight,
    behavior: 'smooth'
  });
  await waitForTime(time);
}

export const clickSelector = async (selector) => {
  await waitForTime(WAIT_TIME_FOR_CLICK_EVENT);
  selectElement(selector).click();
  await waitForTime(WAIT_TIME_FOR_CLICK_EVENT);
}

export const waitForElementToClose = async (selector) => {
  while (selectElement(selector)) {
    await waitForTime(WHILE_LOOP_WAIT_TIME);
  }
}

export const waitForElementToOpen = async (selector) => {
  while (!selectElement(selector)) {
    await waitForTime(WHILE_LOOP_WAIT_TIME);
  }
}

export const compareFirstChildAndScroll = async (element) => {
  let firstChildHref = element.querySelector('a').href;
  await scrollAndWaitFotTime(element, 500, WAIT_TIME_FOR_SCROLL_EVENT);
  let firstChildHrefAfterScroll = element.querySelector('a').href;

  // If first child is not changed after scroll then scroll up single time
  if (firstChildHref === firstChildHrefAfterScroll) {
    await scrollAndWaitFotTime(element, -100, WAIT_TIME_FOR_SCROLL_EVENT);
  }
}